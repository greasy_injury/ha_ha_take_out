import {
    CLEAR_CART,
    DECREMENT_FOOD_COUNT, INCREMENT_FOOD_COUNT,
    RECEIVE_ADDRESS,
    RECEIVE_CATEGORYS, RECEIVE_GOODS,
    RECEIVE_INFO, RECEIVE_RATINGS,
    RECEIVE_SHOPS,
    RECEIVE_USER_INFO,
    RESET_USER_INFO, UAER_ADDRESS
} from "./mutation-type";
import Vue from 'vue'

export default {
    [RECEIVE_ADDRESS](state, {address}) {
        state.address = address
    },

    [RECEIVE_CATEGORYS](state, {categorys}) {
        state.categorys = categorys
    },

    [RECEIVE_SHOPS](state, {shops}) {
        state.shops = shops
    },
    [RECEIVE_USER_INFO](state, {userInfo}) {
        state.userInfo = userInfo
    },
    [RESET_USER_INFO](state) {
        state.userInfo = {}
    },
    [RECEIVE_INFO](state, {info}) {
        state.info = info
    },
    [RECEIVE_RATINGS](state, {ratings}) {
        state.ratings = ratings
    },
    [RECEIVE_GOODS](state, {goods}) {
        state.goods = goods
    },
    [INCREMENT_FOOD_COUNT](state, {food}) {
        if(!food.count) { // 第一次增加
            // food.count = 1  // 新增属性(没有数据绑定)
            /*
            对象
            属性名
            属性值
             */
            Vue.set(food,'count',1);
            // 将food添加到cartFoods中
            state.cartFoods.push(food)
        } else {
            food.count++
        }
    },
    [DECREMENT_FOOD_COUNT](state, {food}) {
        if(food.count) {// 只有有值才去减
            food.count--;
            if(food.count===0) {
                // 将food从cartFoods中移除
                state.cartFoods.splice(state.cartFoods.indexOf(food), 1)
            }
        }

    },
    [CLEAR_CART](state){
        state.cartFoods.forEach(food=>{
           Vue.delete(food,'count')
        });
        state.cartFoods=[]
    },

    [UAER_ADDRESS](state, {userAddress}) {
        state.userAddress = userAddress
    },
}
