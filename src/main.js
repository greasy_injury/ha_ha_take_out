import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import css  from '../static/css/reset.css'
import VueLazyLoad from 'vue-lazyload'
import loading from  './components/ShopList/images/timg.gif'
import error from  './components/ShopList/images/timg.jpg'
import { Button, Cell } from 'mint-ui'

import './mock/mockServer'
Vue.component(Button.name, Button);
Vue.component(Cell.name, Cell);

Vue.use(VueLazyLoad,{
  preLoad: 1.3,
  error,
  loading,
  attempt: 1
});

Vue.config.productionTip = false;

new Vue({
  router,
  css,
  store,
  render: h => h(App)
}).$mount('#app');
