export  default {
    totalNumber(state){
       return  state.cartFoods.reduce((prev,food)=>{return prev+food.count},0)
    },
    totalPrice(state){
       return  state.cartFoods.reduce((preTotal, food) => preTotal + food.count*food.price , 0)
    }
}
