module.exports={

    devServer: {

        proxy: {

            '/api': {

                target: 'http://localhost:4000',

                ws: true,

                changeOrigin: true,

                pathRewrite: {

                    '^/api': ''  //通过pathRewrite重写地址，将前缀/api转为/

                }

            }

        }

    },


    css: {
        loaderOptions: {
            css: {
                // 这里的选项会传递给 css-loader
                importLoaders: 1,
            },
        }
    }

}
