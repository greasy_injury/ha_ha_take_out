import Vue from 'vue'
import VueRouter from 'vue-router'
import Msite from '../views/Msite/Msite.vue'
import Shop from '../views/Shop/Shop'
import ShopGoods from '../views/Shop/ShopGoods'
import ShopInfo from '../views/Shop/ShopInfo'
import ShopAcss from '../views/Shop/ShopAcss'
import PAddress from '../views/Profile/ProfileAddress'
Vue.use(VueRouter);

  const routes = [
  {
    path: '/msite',
    name: 'Msite',
    component: Msite,
    meta:{
      showfooder:true
    }
  },
  {
    path: '/order',
    name: 'Order',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Order/Order.vue'),
    meta:{
      showfooder:true
    }
  },
    {
      path: '/profile',
      name: 'Profile',
      component: () => import(/* webpackChunkName: "about" */ '../views/Profile/Profile.vue'),
      meta:{
        showfooder:true
      }
    },
    {
      path: '/search',
      name: 'Search',
      component: () => import(/* webpackChunkName: "about" */ '../views/Search/Search.vue'),
      meta:{
        showfooder:true
      }
    },
    {
      path: '/login',
      name: 'Login',
      component: () => import(/* webpackChunkName: "about" */ '../views/Login/Login.vue')
    },
    {
      path: '/shop',
      name: 'Shop',
      component: Shop,
      children:[
        {
          path: '/shop/info',
          name: 'ShopInfo',
          component: ShopInfo
        },
        {
          path: '/shop/goods',
          name: 'ShopGoods',
          component: ShopGoods
        },
        {
          path: '/shop/acss',
          name: 'ShopAcss',
          component: ShopAcss
        },
        {
          path:'',
          redirect: '/shop/goods'
        }
      ]
    },
    {
      path: '/address',
      name: 'Address',
      component: PAddress
    },
    {
      path: '/changeAddress',
      name: 'changeAddress',
      component: () => import(/* webpackChunkName: "about" */ '../views/Profile/ChangeAddress')
    },

    {
      path:'/',
      redirect: '/msite'
    }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router
